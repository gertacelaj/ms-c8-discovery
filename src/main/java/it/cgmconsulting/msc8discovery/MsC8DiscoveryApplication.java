package it.cgmconsulting.msc8discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaClient
@SpringBootApplication
public class MsC8DiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsC8DiscoveryApplication.class, args);
    }

}
